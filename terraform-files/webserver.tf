terraform {
  backend "s3" {
    bucket                      = "xy0-bucket"
    key                         = "tfstate_files/terraform.tfstate"
    region                      = "us-east-1"
    endpoint                    = "https://nyc3.digitaloceanspaces.com"
    skip_credentials_validation = true
    skip_get_ec2_platforms      = true
    skip_requesting_account_id  = true
    skip_metadata_api_check     = true
  }
}

resource "digitalocean_droplet" "www_xy0_org" {
  image              = "coreos-stable"
  name               = "www.xy0.org"
  region             = "sfo2"
  size               = "s-1vcpu-1gb"
  private_networking = true

  ssh_keys = [
    "e8:21:23:a9:ec:9f:be:6a:3a:f0:b6:b3:eb:73:2d:45",
    "de:af:c2:6b:b2:9c:89:26:85:f8:28:1a:a6:d0:71:63",
    "79:94:64:68:b0:ef:ab:07:40:dc:d6:73:e1:08:70:9b",
  ]

  connection {
    user        = "core"
    type        = "ssh"
    private_key = "${file(var.pvt_key)}"
    timeout     = "2m"
    agent       = false
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mkdir /web",
      "sudo chown -R core:core /web",
    ]
  }

  provisioner "file" {
    source      = "${var.code_dir}/public"
    destination = "/web"
  }

  #provisioner "file" {
  #  source      = "${var.code_dir}/system_files/blog.service"
  #  destination = "/etc/systemd/system/blog.service"
  #}

  provisioner "remote-exec" {
    inline = [
      "sudo chown -R root:root /web",
      "sudo find /web -type d -exec chmod 755 {} +",
      "sudo find /web -type f -exec chmod 444 {} +",
      "sudo docker run -d -v /web/public:/usr/share/nginx/html -p 80:80 nginx",
    ]
  }
}
