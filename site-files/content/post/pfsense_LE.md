+++
title = "Using Let's Encrypt with PFSense"
date = 2018-06-21T10:13:25-04:00
categories = ["Let's Encrypt", "PFSense"]
description = ""
draft = true
tags = ["Let's Encrypt", "PFSense"]
images = []
+++

Today I finally got fed up with accepting the security warning when logging in
to my router. At home I run [PFSense](https://www.pfsense.org/) on a refurbished
IBM ThinkCentre desktop PC using an Intel Pro/1000 4 port network card as my
router (It's overkill but it's dead simple and it just works without needing to
worry about system resource exhaustion or proprietary components being
unavailable when it breaks). I decided to setup Let's Encrypt to fix this.

There's a really nice package for PFSense already available for install called
"ACME". It can be installed from `System/Package Manager`. Once installed,
configuration is fairly straight forward. In summary, configure an ACME account
key, configure a NAT rule to forward incoming LE DNS requests on port 80 to the
ACME service and request a certificate.

# Setup Account Key

Under `Services/Acme/Accountkeys` click on the [+Add] button. Enter a name and
optional Description. Under `Acme Server` make sure you select one of the
servers labeled "production" (I chose v2 since it's newer and more capable but
either would work) and supply a valid email address. Once filled out, click the
button for [+ Create new account key]. Once that process finishes, click on
[Register acme account key] followed by [Save].

# Setup Port-forward Rule

Next, we need to forward requests on port 80 to the Acme service that we'll be
running on port 8082. If you already have something on port 80, you may need to
configure the certificate service differently. This is not going to be covered
here as it varies widely depending on what you already have configured. So, I
hope you don't have anything already running on port 80 :)

Under `Firewall/NAT/Port Forward` add a new rule as described below (Any options
skipped are unimportant and can be ignored):

```
Interface: WAN
Protocol: TCP
Destination Port Range:
  From Port: HTTP
  To Port: HTTP
Redirect Target IP: 192.168.1.1 (or your router IP if different)
Redirect Target Ports:
  Port: Other
  Custom: 8082
Description: ACME
```

Save and apply the new rule

# Setup Certificate

Go to `Services/Acme/Certificates` and click on [+ Add]. Provide a Name and
Description for your reference (can be anything). Status should be set to
Active. `Acme Account` should already be selected to the Account you created in
the first step. Private Key should be at least 2048-bit-RSA; higher is better.
The next part is a little tricky, we'll configure the `Domain SAN list` as shown
below:

```
Mode: Enabled
Domainname: yourdomain.com (whatever you want the certificate to be valid for)
Method: Standalone HTTP Server
Port: 8082
```

Next click the [+ Add] button, scroll down and click [Save]. This will take you
back to the main `Services/Acme/Certificates` screen. Click on the [Issue/Renew]
button. After a few moments, you should get a green screen with a log of what
was done. If it's not green, something went wrong. Read the message and good
luck. I didn't run in to problems here so I don't know what might happen. Just
double check your settings and Google your error message ;)

# Use the Certificate

Navigate to `System/Advanced/Admin Access` and choose the new certificate under
the `SSL Certificate` field. Save this setting, wait for the service to reload
and you should be back to your main router home page. Unfortunately, you will
probably notice that the cert still shows invalid. This is because we're
accessing the router via it's IP address and our certificate is only valid for
the domain name we configured in the third step. We need to access the router
via this domain name to avoid the certificate warning. But first, we need to
override the DNS entry for our domain on the local network so that it will go to
the router instead of whatever site we normally have at that location. (I know
you're not exposing your router admin console on the WAN port... right?)

# Override DNS

I run a DNS Resolver on my PFSense box. If you use the DNS Forwarder, your steps
will be similar but the wording may be a little different. Try to figure it out,
if you can't, Google "PFSense Forwarder Domain Override" and it should help ;)

Under `Services/DNS Resolver/General Settings` scroll to the bottom and you'll
see a box labeled `Domain Overrides`. We'll need to add an entry here to point
our domain to our router IP. This override will only affect hosts on the network
and we're, once again, assuming you don't need this domain name for anything
else on your local network. This will hog it all. There's probably a better way
to do this but I didn't need to figure that out, thankfully. If you do decide to
figure out how to do split DNS, please contact me and let me know. I'd be
interested in changing my config.

# Wait, but there's more

So, at this point, the domain resolves and it takes you to your router (or it
should) but you've probably noticed there's an error displayed warning you of a
DNS rebinding attack. This is something PFSense has in place by default and it's
a good thing, usually. We need to set an alternate hostname for our router in
the PFSense config.

Navigate to `System/Advanced/Admin Access` and scroll down to the option named
**Alternate Hostnames**. Set your dns name there and save the settings.

That's it. You should now have the ability to navigate to https://YourDomain.com
to access your router without receiving any SSL errors.
